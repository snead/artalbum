/*
 * 
 * Browser Service
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .factory('browserAgent', browserAgent);
 
 	browserAgent.$inject = ['$window'];
 
 	function browserAgent($window) {
 
 		var service = {
 			brand: brand,
 			aca: 'aaaaa'
 		};
 
 		return service;
 
 		function brand() {

 			var userAgent = $window.navigator.userAgent;

	        var browsers = {chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i};

	        for(var key in browsers) {
	            if (browsers[key].test(userAgent)) {
	                return key;
	            }
	       };

	       return 'unknown'; 
 		};
 	}
 })();