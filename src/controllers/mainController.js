/*
 * 
 * Main controller for
 * artalbum angular app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
    'use strict';

    angular
    	.module('app')
    	.controller('mainController', mainController);

    mainController.$inject = ['$location','$rootScope', '$Auth'];

    function mainController($location, $rootScope, $Auth) {

    	/*
         *  Main Scope for
         *  child views and controllers
         *  and other variables
         */
    	    var _scope = this;

            // Scope vars
            _scope._    = _;            
            _scope.nav  = _config.nav; 
            _scope.isLogged = $Auth.checkLogged();

        /*
         *  On route change event
         */    
            $rootScope.$on('$routeChangeStart',
            function (event, next, current) {
               
                // Show admin nav link?
                _scope.isLogged = $Auth.checkLogged();
                if ( $location.url() === '/login'
                    || $location.url() === '/admin'
                    || _scope.isLogged) {                     
                    $Auth.showAdminNav = true;
                }

                // if route requires auth and user is not logged in
                if (!$Auth.hasAccess($location.url())) {
                    $location.path('/login');
                }                
            });
	}
 
 })();