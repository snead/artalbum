<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/*
	 * 
	 * ArtAlbum User authentication API Controller
	 *
	 * @author: Francisco Rader
	 * @email: franciscorader@gmail.com
	 * @project: ArtAlbum
	 * @git: https://gitlab.com/snead/artalbum.git
	 * @license: GPL
	 *  
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('User');
    }

	public function auth()
	{
		$this->output->set_content_type('text/json');
		
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$email = $request->email;
		$password = md5($request->password);

		$user = $this->User->getUser($email);
		$token = md5($email . '.' . time() . '.' . rand(1,100));
		if ($user) {
			if ($user->password == $password) {

				$userdata = array(
					'name' => $user->name,
					'id' => $user->user_id,
					'is_logged' => true,
					'token' => $token
				);

				$this->session->set_userdata($userdata);

			} else {

				$userdata = array(
					'name' => null,
					'id' => null,
					'is_logged' => false
				);

				$this->session->set_userdata($userdata);
				$this->output
					->set_status_header(410)
					->set_header('HTTP/1.1 410 BAD CREDENTIALS');
			}
		} else {
			$this->output
				->set_status_header(411)
				->set_header('HTTP/1.0 411 USER DOESNT EXIST')
				->_display();
				exit;
		}

		echo json_encode($this->session->userdata);
	}

	function logout()
	{
		if ($this->session->userdata('is_logged') === true) {
			$this->session->sess_destroy();
			$this->output->set_content_type('text/json');
			echo json_encode(array('statusText' => 'Successfully logged out'));
		} else {
			$this->output->set_status_header(412)
			             ->set_header('HTTP/1.1 412 NO SESSION FOUND');
		}
	}

	function tokenAuth()
	{
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$token = $request->token;

		if ($this->session->userdata('token') !== $token) {

			$this->output->set_status_header(201)
						 ->set_header('HTTP/1.1 201 INVALID TOKEN');
			exit;
		} else {
			$this->output->set_status_header(200)
						 ->set_header('HTTP/1.1 200 VALID TOKEN');
			exit;
		}
	}
}
