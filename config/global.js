/*
 * 
 * ArtAlbum
 * Configuration file
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */

const _env = 
/*GRUNT_INCLUDE_ENVIRONMENT_VARS*/
;

var _log = function(msg, msg2, msg3) {
  if(_env._DEBUG) {
    console.log(msg);
    if(msg2) console.log(msg2);
    if(msg3) console.log(msg3);
  }
}

var _config = {

  // API URLs
  api: {
    contentUrl: _env._API_BASE_URL + '/content',
    authUrl:  _env._API_BASE_URL + '/login',
    insertDataUrl: _env._API_BASE_URL + '/post',
  },

  mediaUrl: {
    'images': '/uploaded/images'
  },

  // Basic conf
  basic: {
    'siteTitle': 'artalbum app'
  },

  // Navigation config
  nav: {
      links: {
          admin: {
              path: '/admin',
              class: 'text-danger',
              label: _.links.admin,
              _scopeAccessRequire: 'isLogged',
              _scopeNgShow: 'showAdminNav'
          },
          home: {
              path: '/home',
              label: _.links.home
          },
          login: {
            path: '/login',
            _scopeNgShow: 'isLogged'
          }
      }
  }
}