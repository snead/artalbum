/* The following module was written by 
 * https://github.com/RevillWeb/angular-preload-image
*/
angular.module('app').directive('imagePreloader', ['$Images', function ($Images) {
    return {
        restrict: 'A',
        terminal: true,
        priority: 100,
        link: function (scope, element, attrs) {
            scope.default = attrs.defaultImage || "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wEWEygNWiLqlwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY/j//z8ABf4C/tzMWecAAAAASUVORK5CYII=";
            attrs.$observe('ngSrc', function () {
                var url = attrs.ngSrc;
                attrs.$set('src', url);
            });
        }
    };
}]);