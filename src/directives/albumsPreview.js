/*
 * 
 * Albums preview
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: project
 * @git: git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .directive('albumsPreview', albumsPreview);
 
 	albumsPreview.$inject = [];
 
 	function albumsPreview() {
 
 	    var directive = {
 		    restrict: 'A',
 		    templateUrl: 'app/templates/directives/albums-preview.html',
 		    scope: {},
 		    controller: albumsPreviewController,
 		    controllerAs: 'vm',
 	        bindToController: true
 	    };
 
 	    return directive;
 	}
 
 	albumsPreviewController.$inject = ['$Albums','$Images','$location'];
 
 	function albumsPreviewController($Albums, $Images, $location) {
 		// Shared vm
 		var vm = this;
 		vm.albums = [];

 		vm.goToAlbum = function ( url ) {
 			var path = 'album/' + url;
 			console.log(path);
 			$location.path( path );
 		};

 		$Albums.refresh().then(function(data){
 			angular.forEach(data, function(album) {
 				vm.albums.push({
 					title: album.title,
 					cover: album.images[0],
 					title_url: album.title_url,
 					album_id: album.album_id
 				});
 			});
 		});
 	}
 })();