const _env = {
    _LANGUAGE: "ES",
    _API_BASE_URL: "http://artalbum.dev/api/index.php",
    _DEBUG: true
};

var _log = function(msg, msg2, msg3) {
    if (_env._DEBUG) {
        console.log(msg);
        if (msg2) console.log(msg2);
        if (msg3) console.log(msg3);
    }
};

var _config = {
    api: {
        contentUrl: _env._API_BASE_URL + "/content",
        authUrl: _env._API_BASE_URL + "/login",
        insertDataUrl: _env._API_BASE_URL + "/post"
    },
    mediaUrl: {
        images: "/uploaded/images"
    },
    basic: {
        siteTitle: "artalbum app"
    },
    nav: {
        links: {
            admin: {
                path: "/admin",
                "class": "text-danger",
                label: _.links.admin,
                _scopeAccessRequire: "isLogged",
                _scopeNgShow: "showAdminNav"
            },
            home: {
                path: "/home",
                label: _.links.home
            },
            login: {
                path: "/login",
                _scopeNgShow: "isLogged"
            }
        }
    }
};