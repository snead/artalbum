/*
 * 
 * Authentication Service
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .factory('$Auth', $Auth);
 
 	$Auth.$inject = ['$q','$cookies','apiRequestService','$location'];
 
 	function $Auth($q, $cookies, apiRequestService, $location) {
 
 		var service = {
 			hasAccess: hasAccess,
 			checkLogged: checkLogged,
 			logout: logout,
 			login: login,
            getUsername: getUsername
 		};

 		service.nav = _config.nav;
 
 		return service;
 
 		function hasAccess(route) {
 			var access = true;
            angular.forEach(service.nav.links, function(val, key) {
                if (route === val.path) {                        
                    service.nav.links[key].selected = true;
                    if (val.serviceAccessRequire !== undefined) {                           
                        access = service[val.serviceAccessRequire];                            
                    }
                } else {
                    service.nav.links[key].selected = false;
                }
            });
            return access;
 		};

 		function checkLogged() {
            if ($cookies.getObject('sess') !== undefined) {
                console.info($cookies.getObject('sess'));
                return apiRequestService
                .tokenAuth($cookies.getObject('sess').token)
                .then(function _success(res) {
                    if (res.status !== 200) {
                        return false;
                    } else {                        
                        service.showAdminNav = true;
                        return true;
                    }
                });
            }
            return false;
        };

        function login( email , password) {
            
    		return apiRequestService
    			.authenticate(email, password)
    			.then(_success, _failure);

            function _success(response) {
                $cookies.putObject('sess', response.data);
                $location.path('/');
                service.showAdminNav = true;
                return response;
            };

            function _failure(response) {
            	return $q.reject(response);
            };
    	};

        function logout() {
    		apiRequestService.deAuthenticate();
            $cookies.remove('sess');
            $cookies.remove('sessid');
            service.showAdminNav = false;
    		$location.path('/#/admin');
    	};

        function getUsername() {

            var sess = $cookies.getObject('sess');

            if (sess !== undefined) {
                return sess.name;
            }

            return false;
        }
 	}
 })();