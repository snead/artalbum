(function() {
    "use strict";
    angular.module("app").controller("adminController", adminController);
    adminController.$inject = [];
    function adminController() {
        var vm = this;
    }
})();

(function() {
    "use strict";
    angular.module("app").controller("albumController", albumController);
    albumController.$inject = [ "$Auth", "$Albums", "$Images", "$routeParams" ];
    function albumController($Auth, $Albums, $Images, $routeParams) {
        var vm = this;
        vm.albumUrl = $routeParams.albumUrl;
        vm.imageId = $routeParams.imageId;
        vm.images = [];
        vm.loaded = false;
        vm.isLoading = true;
        vm.percentLoaded = 1;
        vm.isLogged = $Auth.checkLogged();
        vm.updating = false;
        vm.updateMessage = null;
        vm.labels = _.album;
        vm.getAlbum = getAlbum;
        vm.doUpdate = doUpdate;
        init();
        function init() {
            getAlbum();
        }
        vm.sortableOptions = {
            orderChanged: function(event) {
                var destFile = event.dest.sortableScope.modelValue[event.dest.index];
                var srcFile = event.source.itemScope.sortableScope.modelValue[event.source.index];
                $Images.syncOrder(destFile, srcFile).then(function(res) {
                    $Albums.refresh(true).then(function(albums) {
                        angular.forEach(albums, function(album) {
                            if (album.title_url === vm.albumUrl) {
                                vm.album.images = album.images;
                            }
                        });
                    });
                }).catch(function(err) {
                    console.log(err);
                });
            }
        };
        if (vm.isLogged === true) {
            vm.sortableOptions.containment = ".gallery";
        }
        function doUpdate() {
            vm.updating = true;
            if (_debug) {
                console.info("trying to update: ", vm.album, vm.album.album_id);
            }
            $Albums.saveAlbum(vm.album, vm.album.album_id).then(_success, _failure);
            function _success() {
                vm.updating = false;
                vm.updateMessage = 1;
            }
            function _failure() {
                vm.updating = false;
                vm.updateMessage = vm.labels.form.editFailure;
            }
        }
        function getAlbum() {
            $Albums.refresh().then(function(data) {
                angular.forEach(data, function(album) {
                    if (album.title_url === vm.albumUrl) {
                        vm.album = album;
                        var imageLocations = [];
                        angular.forEach(album.images, function(image) {
                            imageLocations.push(_config.mediaUrl.images + "/" + image.filename);
                        });
                        if (imageLocations.length > 0) {
                            $Images.download(imageLocations).then(function handleResolve(imageLocations) {
                                vm.isLoading = false;
                                vm.loaded = true;
                                if (_debug) {
                                    console.info("Images load was successful");
                                }
                            }, function handleReject(imageLocation) {
                                vm.isLoading = false;
                                vm.loaded = false;
                                if (_debug) {
                                    console.error("Images loading Failed", imageLocation);
                                    console.info("Images load Failure");
                                }
                            }, function handleNotify(event) {
                                vm.percentLoaded = event.percent;
                                if (_debug) {
                                    console.info("Images percent loaded:", event.percent);
                                }
                            });
                        } else {
                            vm.loaded = true;
                            vm.isLoading = false;
                            vm.noImagesFound = true;
                        }
                    }
                });
            });
        }
    }
})();

(function() {
    "use strict";
    angular.module("app").controller("homeController", homeController);
    homeController.$inject = [ "$scope", "$interval" ];
    function homeController($scope, $interval) {
        var vm = this;
        var _scope = $scope.$parent._scope;
        init();
        function init() {}
    }
})();

(function() {
    "use strict";
    angular.module("app").controller("loginController", loginController);
    loginController.$inject = [ "$Auth", "$location" ];
    function loginController($Auth, $location) {
        var vm = this;
        vm.email = null;
        vm.password = null;
        vm.waiting = false;
        vm.message = null;
        vm.login = login;
        function login() {
            vm.waiting = true;
            $Auth.login(vm.email, vm.password).then(_success, _failure);
            function _success(response) {
                vm.isLogged = true;
            }
            function _failure(response) {
                _log(response.status);
                vm.waiting = false;
                if (response.status === 410) {
                    vm.message = _.login.badPassword;
                }
                if (response.status === 411) {
                    vm.message = _.login.userDoesntExist;
                }
            }
        }
    }
})();

(function() {
    "use strict";
    angular.module("app").controller("mainController", mainController);
    mainController.$inject = [ "$location", "$rootScope", "$Auth" ];
    function mainController($location, $rootScope, $Auth) {
        var _scope = this;
        _scope._ = _;
        _scope.nav = _config.nav;
        _scope.isLogged = $Auth.checkLogged();
        $rootScope.$on("$routeChangeStart", function(event, next, current) {
            _scope.isLogged = $Auth.checkLogged();
            if ($location.url() === "/login" || $location.url() === "/admin" || _scope.isLogged) {
                $Auth.showAdminNav = true;
            }
            if (!$Auth.hasAccess($location.url())) {
                $location.path("/login");
            }
        });
    }
})();

(function() {
    angular.module("app").factory("$Albums", $Albums);
    $Albums.$inject = [ "apiRequestService", "$q", "$interval", "$Images", "$cookies" ];
    function $Albums(apiRequestService, $q, $interval, $Images, $cookies) {
        var service = {
            refresh: refresh,
            all: null,
            saveAlbum: saveAlbum
        };
        return service;
        function refresh(force) {
            if (service.all === null || force) {
                var Request = apiRequestService.makeRequest(_config.api.contentUrl + "/albums", "GET");
                return Request.then(_success, _failure);
            } else {
                var deferred = $q.defer();
                deferred.resolve(service.all);
                return deferred.promise;
            }
            function _success(res) {
                _log(res.status, res.statusText);
                var albums = res.data;
                service.all = albums;
                _log(service.all, "----------------");
                return $Images.refresh(true).then(function(images) {
                    angular.forEach(albums, function(album, albumKey) {
                        service.all[albumKey].images = [];
                        angular.forEach(images, function(image) {
                            if (image.parent_id === album.album_id) {
                                service.all[albumKey].images.push(image);
                            }
                        });
                    });
                    return service.all;
                }, function() {
                    return service.all;
                });
            }
            function _failure(res) {
                return $q.reject(res.data);
            }
        }
        function deleteAlbum(id) {
            return apiRequestService.makeRequest(_config.api.insertDataUrl + "/album", "DELETE", {
                token: $cookies.getObject("sess").token,
                album_id: id
            });
        }
        function saveAlbum(data, id) {
            var album = {};
            album.title = data.title || null;
            album.date = data.date || null;
            album.text = data.text || null;
            album.token = $cookies.getObject("sess").token;
            var deferred = $q.defer();
            var insertDataUrl = _config.api.insertDataUrl + "/album";
            if (id) {
                insertDataUrl = insertDataUrl + "/" + id;
            }
            if (album.title !== null) {
                var Request = apiRequestService.makeRequest(insertDataUrl, "POST", album);
                return Request.then(_success, _failure);
                function _success(res) {
                    _log(res);
                    return res;
                }
                function _failure(res) {
                    deferred.resolve(res);
                    return deferred.promise;
                }
            } else {
                deferred.resolve(_.album.emptyTitle);
                return deferred.promise;
            }
        }
    }
})();

(function() {
    angular.module("app").factory("$Images", $Images);
    $Images.$inject = [ "$q", "$interval", "apiRequestService", "$rootScope", "$cookies" ];
    function $Images($q, $interval, apiRequestService, $rootScope, $cookies) {
        var service = {
            refresh: refresh,
            all: null,
            Preloader: Preloader,
            preloadImages: preloadImages,
            upload: upload,
            download: download,
            syncOrder: syncOrder
        };
        function refresh(force) {
            if (service.all === null || force) {
                var Request = apiRequestService.makeRequest(_config.api.contentUrl + "/images", "GET");
                return Request.then(_success, _failure);
            } else {
                var deferred = $q.defer();
                deferred.resolve(service.all);
                return deferred.promise;
            }
            function _success(res) {
                _log(res.status, res.statusText);
                service.all = res.data;
                _log(service.all, "----------------");
                return service.all;
            }
            function _failure(res) {
                return $q.reject(res.data);
            }
        }
        function deleteImage(id) {
            return apiRequestService.makeRequest(_config.api.insertDataUrl + "/image", "DELETE", {
                token: $cookies.getObject("sess").token,
                file_id: id
            });
        }
        function syncOrder(from, to) {
            var fromId = from["file_id"];
            var toId = to["file_id"];
            var req = apiRequestService.makeRequest(_config.api.insertDataUrl + "/syncOrder/" + fromId + "/" + toId, "POST", {
                token: $cookies.getObject("sess").token
            });
            return req;
        }
        function upload(file, albumid) {
            var deferred = $q.defer();
            var formData = new FormData();
            formData.append("token", $cookies.getObject("sess").token);
            formData.append("file", file);
            formData.append("album_id", albumid);
            return apiRequestService.makeRequest(_config.api.insertDataUrl + "/image", "POST", formData, {
                "Content-Type": undefined
            });
        }
        function download(locations) {
            return preloadImages(locations);
        }
        function Preloader(imageLocations) {
            this.imageLocations = imageLocations;
            this.imageCount = this.imageLocations.length;
            this.loadCount = 0;
            this.errorCount = 0;
            this.states = {
                PENDING: 1,
                LOADING: 2,
                RESOLVED: 3,
                REJECTED: 4
            };
            this.state = this.states.PENDING;
            this.deferred = $q.defer();
            this.promise = this.deferred.promise;
        }
        function preloadImages(imageLocations) {
            var preloader = new service.Preloader(imageLocations);
            return preloader.load();
        }
        Preloader.prototype = {
            constructor: Preloader,
            isInitiated: function isInitiated() {
                return this.state !== this.states.PENDING;
            },
            isRejected: function isRejected() {
                return this.state === this.states.REJECTED;
            },
            isResolved: function isResolved() {
                return this.state === this.states.RESOLVED;
            },
            load: function load() {
                if (this.isInitiated()) {
                    return this.promise;
                }
                this.state = this.states.LOADING;
                for (var i = 0; i < this.imageCount; i++) {
                    this.loadImageLocation(this.imageLocations[i]);
                }
                return this.promise;
            },
            handleImageError: function handleImageError(imageLocation) {
                this.errorCount++;
                if (this.isRejected()) {
                    return;
                }
                this.state = this.states.REJECTED;
                this.deferred.reject(imageLocation);
            },
            handleImageLoad: function handleImageLoad(imageLocation) {
                this.loadCount++;
                if (this.isRejected()) {
                    return;
                }
                this.deferred.notify({
                    percent: Math.ceil(this.loadCount / this.imageCount * 100),
                    imageLocation: imageLocation
                });
                if (this.loadCount === this.imageCount) {
                    this.state = this.states.RESOLVED;
                    this.deferred.resolve(this.imageLocations);
                }
            },
            loadImageLocation: function loadImageLocation(imageLocation) {
                var preloader = this;
                var image = $(new Image()).load(function(event) {
                    $rootScope.$apply(function() {
                        preloader.handleImageLoad(event.target.src);
                        preloader = image = event = null;
                    });
                }).error(function(event) {
                    $rootScope.$apply(function() {
                        preloader.handleImageError(event.target.src);
                        preloader = image = event = null;
                    });
                }).prop("src", imageLocation);
            }
        };
        return service;
    }
})();

(function() {
    angular.module("app").factory("apiRequestService", apiRequestService);
    apiRequestService.$inject = [ "$http" ];
    function apiRequestService($http) {
        var service = {
            makeRequest: makeRequest,
            authenticate: authenticate,
            tokenAuth: tokenAuth,
            deAuthenticate: deAuthenticate,
            transformRequest: false
        };
        return service;
        function makeRequest(url, method, data, headers, transformRequest) {
            var request = {
                method: method || "GET",
                url: url || $config.API_URL,
                data: data || {},
                headers: headers || null
            };
            if (service.transformRequest) {
                request.transformRequest = service.transformRequest;
            }
            console.log(request);
            return $http(request);
        }
        function authenticate(email, password) {
            var url = _config.api.authUrl + "/auth";
            var data = {
                email: email,
                password: password
            };
            return service.makeRequest(url, "POST", data);
        }
        function tokenAuth(token) {
            var url = _config.api.authUrl + "/tokenAuth/";
            return service.makeRequest(url, "POST", {
                token: token
            });
        }
        function deAuthenticate() {
            var url = _config.api.authUrl + "/logout/";
            return service.makeRequest(url, "GET");
        }
    }
})();

(function() {
    angular.module("app").factory("$Auth", $Auth);
    $Auth.$inject = [ "$q", "$cookies", "apiRequestService", "$location" ];
    function $Auth($q, $cookies, apiRequestService, $location) {
        var service = {
            hasAccess: hasAccess,
            checkLogged: checkLogged,
            logout: logout,
            login: login,
            getUsername: getUsername
        };
        service.nav = _config.nav;
        return service;
        function hasAccess(route) {
            var access = true;
            angular.forEach(service.nav.links, function(val, key) {
                if (route === val.path) {
                    service.nav.links[key].selected = true;
                    if (val.serviceAccessRequire !== undefined) {
                        access = service[val.serviceAccessRequire];
                    }
                } else {
                    service.nav.links[key].selected = false;
                }
            });
            return access;
        }
        function checkLogged() {
            if ($cookies.getObject("sess") !== undefined) {
                console.info($cookies.getObject("sess"));
                return apiRequestService.tokenAuth($cookies.getObject("sess").token).then(function _success(res) {
                    if (res.status !== 200) {
                        return false;
                    } else {
                        service.showAdminNav = true;
                        return true;
                    }
                });
            }
            return false;
        }
        function login(email, password) {
            return apiRequestService.authenticate(email, password).then(_success, _failure);
            function _success(response) {
                $cookies.putObject("sess", response.data);
                $location.path("/");
                service.showAdminNav = true;
                return response;
            }
            function _failure(response) {
                return $q.reject(response);
            }
        }
        function logout() {
            apiRequestService.deAuthenticate();
            $cookies.remove("sess");
            $cookies.remove("sessid");
            service.showAdminNav = false;
            $location.path("/#/admin");
        }
        function getUsername() {
            var sess = $cookies.getObject("sess");
            if (sess !== undefined) {
                return sess.name;
            }
            return false;
        }
    }
})();

(function() {
    angular.module("app").factory("browserAgent", browserAgent);
    browserAgent.$inject = [ "$window" ];
    function browserAgent($window) {
        var service = {
            brand: brand,
            aca: "aaaaa"
        };
        return service;
        function brand() {
            var userAgent = $window.navigator.userAgent;
            var browsers = {
                chrome: /chrome/i,
                safari: /safari/i,
                firefox: /firefox/i,
                ie: /internet explorer/i
            };
            for (var key in browsers) {
                if (browsers[key].test(userAgent)) {
                    return key;
                }
            }
            return "unknown";
        }
    }
})();

(function() {
    angular.module("app").factory("myRouteParams", myRouteParams);
    myRouteParams.$inject = [ "$rootScope", "$location" ];
    function myRouteParams($rootScope, $location) {
        var service = {
            route: null,
            path: null,
            subpath: null,
            split: null
        };
        service.route = $location.path();
        service.split = service.route.split("/");
        service.path = service.split[1];
        service.subpath = service.split[2];
        $rootScope.$on("$routeChangeStart", function(next, current) {
            service.route = $location.path();
            service.split = service.route.split("/");
            service.path = service.split[1];
            service.subpath = service.split[2];
        });
        return service;
    }
})();

(function() {
    angular.module("app").directive("albumList", albumList);
    function albumList() {
        var directive = {
            restrict: "A",
            templateUrl: "/app/templates/directives/album-list.html",
            scope: {},
            controller: AlbumListController,
            controllerAs: "vm",
            bindToController: true,
            link: link,
            compile: compile
        };
        return directive;
        function compile(tElement, tAttrs, transclude) {
            return {
                pre: preLink,
                post: postLink
            };
            function preLink(scope, element, attrs, controller) {
                scope.vm.getAlbums();
            }
            function postLink(scope, element, attrs, controller) {
                $(element).bind("keydown keypress", function(event) {
                    if (event.which === 13) {
                        scope.$apply(function() {
                            scope.$eval(attrs.myEnter);
                        });
                        event.preventDefault();
                        scope.vm.waiting = true;
                        scope.vm.saveAlbum({
                            title: scope.vm.newAlbumTitle
                        });
                    }
                });
            }
        }
        function link(scope, element, attrs, ctrl) {}
        AlbumListController.$inject = [ "$Albums", "$scope", "$Auth" ];
        function AlbumListController($Albums, $scope, $Auth) {
            var vm = this;
            vm.albums = null;
            vm.currentAlbumDate = null;
            vm.waiting = true;
            vm.getAlbums = getAlbums;
            vm.saveAlbum = saveAlbum;
            vm.newAlbumTitle = null;
            vm._ = {
                album: {
                    newAlbum: _.album.newAlbum
                }
            };
            vm.isLogged = $Auth.checkLogged();
            function getAlbums(force) {
                this.force = force || false;
                vm.waiting = true;
                return $Albums.refresh(this.force).then(function(data) {
                    vm.albums = data;
                    vm.waiting = false;
                    return data;
                });
            }
            function saveAlbum(data, id) {
                if (!data.title || data.title == null) {
                    return _.album.emptyTitle;
                } else {
                    vm.waiting = true;
                    vm.albums = null;
                    this.data = data;
                    this.id = id || null;
                    $Albums.saveAlbum(this.data, this.id).then(_success, _failure);
                    function _success() {
                        vm.getAlbums(true).then(function(data) {
                            data[0].new = true;
                            vm.newAlbumTitle = null;
                        });
                    }
                    function _failure() {
                        return _.album.newAlbumError;
                        vm.waiting = false;
                    }
                }
            }
        }
    }
})();

(function() {
    angular.module("app").directive("albumsPreview", albumsPreview);
    albumsPreview.$inject = [];
    function albumsPreview() {
        var directive = {
            restrict: "A",
            templateUrl: "app/templates/directives/albums-preview.html",
            scope: {},
            controller: albumsPreviewController,
            controllerAs: "vm",
            bindToController: true
        };
        return directive;
    }
    albumsPreviewController.$inject = [ "$Albums", "$Images", "$location" ];
    function albumsPreviewController($Albums, $Images, $location) {
        var vm = this;
        vm.albums = [];
        vm.goToAlbum = function(url) {
            var path = "album/" + url;
            console.log(path);
            $location.path(path);
        };
        $Albums.refresh().then(function(data) {
            angular.forEach(data, function(album) {
                vm.albums.push({
                    title: album.title,
                    cover: album.images[0],
                    title_url: album.title_url,
                    album_id: album.album_id
                });
            });
        });
    }
})();

(function() {
    angular.module("app").directive("fileUpload", fileUpload);
    fileUpload.$inject = [];
    function fileUpload() {
        var directive = {
            restrict: "A",
            templateUrl: "app/templates/directives/file-upload.html",
            scope: {
                albumId: "="
            },
            controller: fileUploadController,
            controllerAs: "vm",
            bindToController: true,
            link: link,
            compile: compile
        };
        return directive;
        function compile(tElement, tAttrs, transclude) {
            return {
                pre: preLink,
                post: postLink
            };
            function preLink(scope, element, attrs, controller) {}
            function postLink(scope, element, attrs, controller) {
                var filesInput = $(element).find("input");
                scope.vm.dropzone = $(element).find(".dropzone");
                filesInput.on("change", filesHandler);
                scope.vm.files = [];
                scope.vm.pendingFiles = 0;
                scope.vm.uploadedFiles = 0;
                scope.vm.failedFiles = 0;
                scope.vm.dropzone.on("dragover", fileDragHandler);
                scope.vm.dropzone.on("dragleave", fileLeaveHandler);
                scope.vm.dropzone.on("drop", filesHandler);
                function filesHandler(event) {
                    fileDragHandler(event);
                    var files = event.originalEvent.target.files || event.originalEvent.dataTransfer.files;
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        var imageType = /^image\//;
                        if (!imageType.test(file.type)) {
                            continue;
                        }
                        var img = document.createElement("img");
                        $(img).addClass("thumbnail");
                        $(img).attr("diskurl", file.name);
                        img.file = file;
                        var exists = false;
                        scope.vm.dropzone.find("img").each(function() {
                            $(this).attr("diskurl");
                            if ($(this).attr("diskurl") == file.name) {
                                exists = true;
                            }
                        });
                        if (!exists) {
                            scope.vm.dropzone.append(img);
                        }
                        var reader = new FileReader();
                        reader.onload = function(aImg) {
                            return function(e) {
                                aImg.src = e.target.result;
                            };
                        }(img);
                        reader.readAsDataURL(file);
                        scope.vm.files.push(file);
                    }
                    _log("Files to upload:", scope.vm.files);
                }
                function fileDragHandler(event) {
                    event.stopPropagation();
                    event.preventDefault();
                    scope.vm.dropzone.addClass("drop-zone-hover");
                }
                function fileLeaveHandler(event) {
                    event.stopPropagation();
                    event.preventDefault();
                    scope.vm.dropzone.removeClass("drop-zone-hover");
                }
            }
        }
        function link(scope, element, attrs, ctrl) {}
    }
    fileUploadController.$inject = [ "$q", "$scope", "browserAgent", "$Images", "$cookies" ];
    function fileUploadController($q, $scope, browserAgent, $Images, $cookies) {
        var vm = this;
        var browser = browserAgent.brand();
        if (browser === "chrome" || browser === "firefox") {
            vm.dropZone = true;
        } else {
            vm.dropZone = false;
        }
        vm._ = _.album.form;
        vm.waiting = false;
        vm.doUpload = function(albumId) {
            var defer = $q.defer();
            var promises = [];
            vm.pendingFilesPreviews = vm.dropzone.find("img");
            vm.dropzone.html(vm._.dropFilesHere);
            vm.waiting = true;
            vm.pendingFiles = vm.files.length;
            vm.percentLoaded = 1;
            angular.forEach(vm.files, function(file) {
                promise = $Images.upload(file, vm.albumId).then(_success, _failure);
                promises.push(promise);
                function _success(res) {
                    _log(res.data);
                    vm.pendingFiles--;
                    vm.uploadedFiles++;
                    vm.percentLoaded = Math.ceil(vm.uploadedFiles / (vm.pendingFiles + vm.failedFiles + vm.uploadedFiles) * 100);
                }
                function _failure(res) {
                    _log(res.data);
                    vm.failedFiles++;
                    vm.pending--;
                    defer.reject(res.statusText);
                }
            });
            $q.all(promises).then(finishUpload, finishUpload);
            function finishUpload() {
                setTimeout(function() {
                    location.reload();
                }, 200);
            }
            return defer;
        };
    }
})();

angular.module("app").directive("imagePreloader", [ "$Images", function($Images) {
    return {
        restrict: "A",
        terminal: true,
        priority: 100,
        link: function(scope, element, attrs) {
            scope.default = attrs.defaultImage || "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wEWEygNWiLqlwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAAMSURBVAjXY/j//z8ABf4C/tzMWecAAAAASUVORK5CYII=";
            attrs.$observe("ngSrc", function() {
                var url = attrs.ngSrc;
                attrs.$set("src", url);
            });
        }
    };
} ]);

(function() {
    angular.module("app").directive("navMenu", navMenu);
    navMenu.$inject = [];
    function navMenu() {
        var directive = {
            restrict: "A",
            templateUrl: "app/templates/directives/nav-menu.html",
            scope: false,
            controller: navMenuController,
            controllerAs: "vm",
            bindToController: true,
            link: link,
            compile: compile
        };
        return directive;
        function compile(tElement, tAttrs, transclude) {
            return {
                pre: preLink,
                post: postLink
            };
            function preLink(scope, iElement, iAttrs, controller) {}
            function postLink(scope, iElement, iAttrs, controller) {}
        }
        function link(scope, element, attrs, ctrl) {}
    }
    navMenuController.$inject = [ "$scope", "$Auth", "myRouteParams", "$location" ];
    function navMenuController($scope, $Auth, myRouteParams, $location) {
        var vm = this;
        vm.basic = _config.basic;
        vm.links = _config.nav.links;
        vm.labels = _;
        vm.auth = $Auth;
        vm.isLogged = $Auth.checkLogged();
        vm.username = $Auth.getUsername();
        vm.currentPath = myRouteParams.path;
        vm.currentSubPath = myRouteParams.subpath;
        $scope.$on("$routeChangeStart", function(next, current) {
            vm.currentPath = myRouteParams.path;
            vm.currentSubPath = myRouteParams.subpath;
        });
    }
})();

(function() {
    angular.module("app").directive("progressBar", progressBar);
    progressBar.$inject = [];
    function progressBar() {
        var directive = {
            restrict: "A",
            templateUrl: "app/templates/directives/progress-bar.html",
            scope: {
                loaded: "=",
                label: "="
            },
            controller: progressBarController,
            controllerAs: "vm",
            bindToController: true
        };
        return directive;
    }
    progressBarController.$inject = [ "$scope" ];
    function progressBarController($scope) {
        var vm = this;
    }
})();

(function() {
    angular.module("app").directive("spinner", spinner);
    spinner.$inject = [];
    function spinner() {
        var directive = {
            restrict: "A",
            templateUrl: "app/templates/directives/spinner.html"
        };
        return directive;
    }
})();

(function() {
    angular.module("app").directive("teather", teather);
    teather.$inject = [];
    function teather() {
        var directive = {
            restrict: "A",
            templateUrl: "app/templates/directives/teather.html",
            replace: true,
            scope: {
                album: "=",
                activeImage: "="
            },
            controller: teatherController,
            controllerAs: "vm",
            bindToController: true,
            link: link,
            compile: compile
        };
        return directive;
        function compile(tElement, tAttrs, transclude) {
            return {
                pre: preLink,
                post: postLink
            };
            function preLink(scope, iElement, iAttrs, controller) {}
            function postLink(scope, element, iAttrs, controller) {
                scope.vm.teatherElement = element;
                $(element).find(".arrow").hide();
                if (scope.vm.images.length > 0) {
                    $(element).modal();
                }
                $(element).find(".modal-dialog").on("mouseenter", function() {
                    arrowHideShow();
                });
                $(element).find(".modal-dialog").on("mouseleave", function() {
                    $(element).find(".arrow").hide();
                });
                $(element).find(".next").on("click", function() {
                    scope.vm.goNext(scope.vm.currentImage);
                    arrowHideShow();
                });
                $(element).find(".previous").on("click", function() {
                    scope.vm.goPrev(scope.vm.currentImage);
                    arrowHideShow();
                });
                $(element).on("keydown", function(event) {
                    if (event.which == 39 && scope.vm.currentImage + 1 < scope.vm.imageCount) {
                        scope.vm.goNext(scope.vm.currentImage);
                        arrowHideShow();
                    }
                });
                $(element).on("keydown", function(event) {
                    if (event.which == 37 && scope.vm.currentImage > 0) {
                        scope.vm.goPrev(scope.vm.currentImage);
                        arrowHideShow();
                    }
                });
                function arrowHideShow() {
                    $(element).find(".arrow").hide();
                    if (scope.vm.currentImage + 1 < scope.vm.imageCount) {
                        $(element).find(".next").show();
                    }
                    if (scope.vm.currentImage > 0) {
                        $(element).find(".previous").show();
                    }
                }
            }
        }
        function link(scope, element, attrs, ctrl) {
            scope.vm.album = scope.album;
        }
    }
    teatherController.$inject = [ "$scope", "$location", "$routeParams" ];
    function teatherController($scope, $location, $routeParams) {
        var vm = this;
        vm.images = vm.album.images || [];
        vm.imageCount = vm.images.length;
        vm.currentImage = $routeParams.imageId || 0;
        vm.setActive = function() {
            angular.forEach($scope.vm.album.images, function(image, key) {
                image.active = false;
                if (vm.currentImage == key) {
                    image.active = true;
                }
            });
        };
        vm.setActive();
        $scope.$parent.vm.openTeather = function(openOnKey) {
            vm.currentImage = openOnKey || 0;
            vm.setActive();
        };
        vm.goPrev = function(current) {
            $scope.$apply(function() {
                vm.images[current].active = false;
                vm.images[parseInt(current - 1)].active = true;
                vm.currentImage--;
            });
        };
        vm.goNext = function(current) {
            $scope.$apply(function() {
                vm.images[current].active = false;
                vm.images[parseInt(current + 1)].active = true;
                vm.currentImage++;
            });
        };
        $scope.$on("$destroy", function() {
            $(".modal-backdrop").hide();
            vm = null;
        });
    }
})();