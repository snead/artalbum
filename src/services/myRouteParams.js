/*
 * 
 * Get Route Parameters Service
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .factory('myRouteParams', myRouteParams);
 
 	myRouteParams.$inject = ['$rootScope','$location'];
 
 	function myRouteParams($rootScope, $location) {
 
 		var service = {
 			route: null,
 			path: null,
 			subpath: null,
 			split: null
 		};

 		service.route = $location.path();
 		service.split = service.route.split('/');
 		service.path = service.split[1];
 		service.subpath = service.split[2];

 		$rootScope.$on('$routeChangeStart', function(next, current) { 
		    service.route = $location.path();
	 		service.split = service.route.split('/');
	 		service.path = service.split[1];
	 		service.subpath = service.split[2];
		});

		return service;
 	}
 })();