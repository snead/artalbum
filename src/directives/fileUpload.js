/*
 * 
 * File Upload Directive
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: project
 * @git: git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .directive('fileUpload', fileUpload);
 
 	fileUpload.$inject = [];
 
 	function fileUpload() {
 
 	    var directive = {
 		    restrict: 'A',
 		    templateUrl: 'app/templates/directives/file-upload.html',
 		    scope: {
 		    	albumId: '='
 		    },
 		    controller: fileUploadController,
 		    controllerAs: 'vm',
 	        bindToController: true,
 		    link: link,
 		    compile: compile
 	    };
 
 	    return directive;
 
 	    function compile(tElement, tAttrs, transclude) {
 	        return {
 	          pre: preLink,
 	          post: postLink
 	        }
 	        function preLink(scope, element, attrs, controller) {	 

 		      
 	     	}
 	        function postLink(scope, element, attrs, controller) {
 	        	
 	        	var filesInput = $(element).find('input');
 	        	scope.vm.dropzone = $(element).find('.dropzone');

 	        	filesInput.on('change', filesHandler);

 	        	scope.vm.files = [];
 	        	scope.vm.pendingFiles = 0;
 	        	scope.vm.uploadedFiles = 0;
 	        	scope.vm.failedFiles = 0;

 	        	scope.vm.dropzone.on('dragover', fileDragHandler);
 	        	scope.vm.dropzone.on('dragleave', fileLeaveHandler);
 	        	scope.vm.dropzone.on('drop', filesHandler);

 	        	function filesHandler(event) 
 	        	{ 	        		
					fileDragHandler(event);
					// fetch FileList object
					var files = event.originalEvent.target.files
						|| event.originalEvent.dataTransfer.files;

					// process all File objects
					for (var i = 0; i < files.length; i++) {
					    var file = files[i];
					    var imageType = /^image\//;
					    
					    if (!imageType.test(file.type)) {
					      continue;
					    }
					    
					    var img = document.createElement('img');
					    $(img).addClass('thumbnail');
					    $(img).attr('diskurl', file.name);
					    img.file = file;
					    var exists = false;
					    scope.vm.dropzone.find('img').each(function(){
					    	$(this).attr('diskurl')
					    	if ($(this).attr('diskurl') == file.name) {
					    		exists = true;
					    	}
					    });

					    if(!exists) {
						    	scope.vm.dropzone.append(img);
						    }
					    
					    var reader = new FileReader();
					    
					    reader.onload = (function(aImg) { 
					    	return function(e) { 
					    		aImg.src = e.target.result; 
					    	}; 
					    })(img);

					    reader.readAsDataURL(file);

					    scope.vm.files.push(file);
					}

					_log('Files to upload:',scope.vm.files);
	 	        }
	 	        function fileDragHandler(event) {
					
					event.stopPropagation();
					event.preventDefault();

					scope.vm.dropzone.addClass('drop-zone-hover');
	 	        }
	 	        function fileLeaveHandler(event) {
					
					event.stopPropagation();
					event.preventDefault();

					scope.vm.dropzone.removeClass('drop-zone-hover');
	 	        }
 	        } 	        
 	    }
 
 	    function link(scope, element, attrs, ctrl) {

 	    }
 
 	}
 
 	fileUploadController.$inject = ['$q','$scope','browserAgent','$Images','$cookies'];
 
 	function fileUploadController($q, $scope, browserAgent, $Images, $cookies) {
 		// Shared vm
 		var vm = this;
 		var browser = browserAgent.brand();
 		if (browser === 'chrome' || browser === 'firefox') {
 			vm.dropZone = true;
 		} else { 			
 			vm.dropZone = false; 		
 		}

 		vm._ = _.album.form;
 		vm.waiting = false;

 		vm.doUpload = function(albumId) {

 			var defer = $q.defer();
    		var promises = [];
    		vm.pendingFilesPreviews = vm.dropzone.find('img');
    		vm.dropzone.html(vm._.dropFilesHere);
 			vm.waiting = true;
 			vm.pendingFiles = vm.files.length;
 			vm.percentLoaded = 1;

 			angular.forEach(vm.files, function(file){

 				promise = $Images
 							.upload(file, vm.albumId)
 							.then(_success, _failure);

 				promises.push(promise);

	            function _success(res) {
	                _log(res.data);
	                vm.pendingFiles--;
	                vm.uploadedFiles++;
	                vm.percentLoaded = Math.ceil(
	                	vm.uploadedFiles / 
	                	(
	                		vm.pendingFiles
	                		+ vm.failedFiles
	                		+ vm.uploadedFiles
	                	)
	                	* 100 );
	            };

	            function _failure(res) {
	                _log(res.data);
	                vm.failedFiles++;
	                vm.pending--;
	                /* TODO 
	                do reject promise and make a q.all failure function
	                that reloads page if not all files failed
	                defer.reject(res.statusText);
	                */
	                defer.reject(res.statusText);
	            };

 			// end foreach 				
 			});

 			$q.all(promises).then(finishUpload, finishUpload);
 			function finishUpload() {
 				/* TODO: save errors on cookie for displaying
 				 * after reload */
 				setTimeout(function(){
 					location.reload();
 				},200);
	        };

 			return defer; 
 		};
 	}
 })();