/*
 * 
 * ArtAlbum
 * Espaniol file
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtAlbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */

// LANGUAGE STRINGS OBJECT
var _ = {
	links: {
		home: 'Inicio'
	},
	login: {
		badPassword: 'Password incorrecto',
		userDoesntExist: 'Usuario inexistente', 
		logout: 'Cerrar Sesion'
	}, 
	admin: {
		'pageTitle': 'Panel de Administracion',
		'loggedMessage': 'Sesion iniciada como '
	},
	loading: 'Cargando',
	album: {
		emptyTitle: 'El titulo no puede estar vacio',
		newAlbum: 'Nuevo album',
		newAlbumError: 'No se pudo crear album',
		noImagesFound: 'No hay imagenes en este album',
		form: {
			editFormTitle: 'Editar album',
			editSuccessful: 'Album editado correctamente.',
			editFailure: 'Hubo un error editando el album.',
			title: 'Titulo',
			date: 'Fecha',
			text: 'Texto',
			route: 'Ruta',
			save: 'Guardar',
			upload: 'Subir',
			uploadFormTitle: 'Subir imagenes',
			reorderTitle: 'Arrastra para cambiar el orden',
			dropFilesHere: 'o arrastra imagenes aca',
			uploading: 'Se estan subiendo las imagenes...'
		}
	}
}