/*
 * 
 * Navigation Menu Directive
 * for ArtAlbum angular app
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: ArtALbum
 * @git: https://gitlab.com/snead/artalbum.git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .directive('navMenu', navMenu);
 
 	navMenu.$inject = [];
 
 	function navMenu() {
 
 	    var directive = {
 		    restrict: 'A',
 		    templateUrl: 'app/templates/directives/nav-menu.html',
 		    scope: false,
 		    controller: navMenuController,
 		    controllerAs: 'vm',
 	        bindToController: true, // because the scope is isolated
 		    link: link,
 		    compile: compile
 	    };
 
 	    return directive;
 
 	    function compile(tElement, tAttrs, transclude) {
 	        return {
 	          pre: preLink,
 	          post: postLink
 	        }
 	        function preLink(scope, iElement, iAttrs, controller) {	         
 	     	}
 	        function postLink(scope, iElement, iAttrs, controller) { 	               
 	        }
 	    }
 
 	    function link(scope, element, attrs, ctrl) { 	    	
 	    }
 
 	}
 
 	navMenuController.$inject = ['$scope','$Auth','myRouteParams', '$location'];
 
 	function navMenuController($scope, $Auth, myRouteParams, $location) {
 		// Shared vm
 		var vm = this;

 		vm.basic	= _config.basic;
 		vm.links 	= _config.nav.links;
 		vm.labels 	= _;
 		vm.auth 	= $Auth;
 		vm.isLogged = $Auth.checkLogged();
 		vm.username = $Auth.getUsername();

 		vm.currentPath = myRouteParams.path;
 		vm.currentSubPath = myRouteParams.subpath;

 		$scope.$on('$routeChangeStart', function(next, current) { 
 			vm.currentPath = myRouteParams.path;
 			vm.currentSubPath = myRouteParams.subpath;
 		});
 	}
 })();