/*
 * 
 * Progress Bar Directive
 *
 * @author: Francisco Rader
 * @email: franciscorader@gmail.com
 * @project: project
 * @git: git
 * @license: GPL
 *  
 */
 (function() {
 	angular
 	    .module('app')
 	    .directive('progressBar', progressBar);
 
 	progressBar.$inject = [];
 
 	function progressBar() {
 
 	    var directive = {
 		    restrict: 'A',
 		    templateUrl: 'app/templates/directives/progress-bar.html',
 		    scope: {
 		    	loaded: '=',
 		    	label: '='
 		    },
 		    controller: progressBarController,
 		    controllerAs: 'vm',
 	        bindToController: true,
 	    };
 
 	    return directive;
 	}
 
 	progressBarController.$inject = ['$scope'];
 
 	function progressBarController($scope) {
 		// Shared vm
 		var vm = this;
 	}
 })();