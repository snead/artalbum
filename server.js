/* We could use this node server file to host the app easily */

var express = require('express');
var app = express();

app.use(express.static('./', {index: 'index.html'}));

app.listen(3000, function () {
  console.log('App ready on port 3000...');
});